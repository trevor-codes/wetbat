package com.wetbat.server.entity.exception;

public class QuoteNotFoundException extends RuntimeException {
    public QuoteNotFoundException(Long id) {
        super("Could not find quote " + id);
    }
}
