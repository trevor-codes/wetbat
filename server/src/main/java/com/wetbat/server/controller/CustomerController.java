package com.wetbat.server.controller;

import com.wetbat.server.entity.Customer;
import com.wetbat.server.entity.Quote;
import com.wetbat.server.entity.exception.CustomerNotFoundException;
import com.wetbat.server.repository.CustomerRepository;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {
    private final CustomerRepository customerRepository;
    CustomerController(CustomerRepository repository) {
        this.customerRepository = repository;
    }

    @GetMapping("/customer/{id}")
    public Customer getCustomer(@PathVariable Long id) {
        return customerRepository.findById(id)
                .orElseThrow(() -> new CustomerNotFoundException(id));
    }

    @GetMapping("/customers")
    public MappingJacksonValue getCustomers() {
        List<Customer> customer = customerRepository.findAll();
        MappingJacksonValue value = new MappingJacksonValue(customer);
        return value;
    }

    @PostMapping("/customer")
    public Customer newCustomer(@RequestBody Customer newCustomer) {
        return customerRepository.save(newCustomer);
    }


}