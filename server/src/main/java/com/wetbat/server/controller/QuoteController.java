package com.wetbat.server.controller;

import com.wetbat.server.entity.exception.QuoteNotFoundException;
import com.wetbat.server.repository.QuoteRepository;
import com.wetbat.server.entity.Quote;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class QuoteController {
    private final QuoteRepository quoteRepository;
    QuoteController(QuoteRepository repository) {
        this.quoteRepository = repository;
    }

    @GetMapping("/quote/{id}")
    public Quote getQuote(@PathVariable Long id) {
        return quoteRepository.findById(id)
                .orElseThrow(() -> new QuoteNotFoundException(id));
    }

    @GetMapping("/quotes")
    public MappingJacksonValue getQuotes() {
        List<Quote> quotes = quoteRepository.findAll();
        MappingJacksonValue value = new MappingJacksonValue(quotes);
        return value;
    }

    @PostMapping("/quote")
    public Quote newQuote(@RequestBody Quote newQuote) {
        return quoteRepository.save(newQuote);
    }


}