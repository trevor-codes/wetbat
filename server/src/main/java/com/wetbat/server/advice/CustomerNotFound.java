package com.wetbat.server.advice;

import com.wetbat.server.entity.exception.CustomerNotFoundException;
import com.wetbat.server.entity.exception.QuoteNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class CustomerNotFound {
    @ResponseBody
    @ExceptionHandler(QuoteNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String customerNotFound(CustomerNotFoundException ex) {
        return ex.getMessage();
    }

}
