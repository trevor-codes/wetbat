package com.wetbat.server.repository;

import com.wetbat.server.entity.Quote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
    public interface QuoteRepository extends JpaRepository<Quote, Long> {
}