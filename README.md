# Wetbat MVP
This is the wetbat MVP. It consists of three layers:

## Client Layer
Made in React + Bootstrap, along with Lodash, chartjs, and FontAwesome. The frontend of the application has a (mostly) fully functioning UI complete with the ability to GET and POST data to and from the server. Allowing you to add new Customers and Quotes.

The Construction of the Client layer is complete with a psuedo "store" to maintain and handle data from the server across the application. Fetching updates when the data is changed.

## Server Layer
Built on Spring Boot + Hibernate, the server is a simple REST API that supports GETting and POSTing. Using Hibernate to manage database access.

## Database
Built using mySQL the database consits of two tables. A main Quote(s) table that stores all the data of a customers Quote, and a Customer table for the customers.

The Customers and Quotes are linked in a One-to-Many relationship, as one customer can have several quotes

![](./screenshot.png)