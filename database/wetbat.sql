CREATE TABLE `customer` (
  `customer_id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `phone_number` varchar(45) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `quote` (
  `quote_id` int NOT NULL AUTO_INCREMENT,
  `departure_location` varchar(45) NOT NULL,
  `departure_date` datetime NOT NULL,
  `destination_location` varchar(45) NOT NULL,
  `destination_date` datetime NOT NULL,
  `travellers` int NOT NULL,
  `quote_customer_id` int NOT NULL,
  `rental` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`quote_id`),
  KEY `fk_quote_customer_idx` (`quote_id`),
  KEY `fk_quote_customer_idx1` (`quote_customer_id`),
  CONSTRAINT `fk_quote_customer` FOREIGN KEY (`quote_customer_id`) REFERENCES `customer` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
SELECT * FROM wetbat.quote;