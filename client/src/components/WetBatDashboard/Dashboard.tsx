import React from "react";
import { getCustomers, getQuotes, postCustomer, postQuote } from "../../api/WetbatAPI";
import { Customer, Quote } from "../standards/interfaces";
import NewCustomer from "./Panels/NewCustomer/NewCustomer";
import NewQuote from "./Panels/NewQuote/NewQuote";
import PanelWrapper from "./PanelWrapper/PanelWrapper";
import ViewCustomers from "./Panels/ViewCustomers/ViewCustomers";
import ViewQuotes from "./Panels/ViewQuotes/ViewQuotes";

import "./Dashboard.css";
import { Popout } from "../modal/Popout";
import { Button } from "react-bootstrap";
import { Destinations } from "./Panels/Destinations/Destinations";
import Welcome from "./Panels/Welcome/Welcome";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFastForward, faPlaneArrival, faQuoteLeft, faSyncAlt, faUserPlus, faUsers } from "@fortawesome/free-solid-svg-icons";
interface IOwnProps{

}

interface IOwnState{
    customers: Array<Customer>
    quotes: Array<Quote>
    loading: boolean

    showModal:boolean

    showCustomerModal: boolean
}

/**
 * Home of the application. A flexbox of panels. Maybe even could be rearrangable.
 * Dashboard handles the State of the application as a sort of psuedo "store", and will refetch the store as soon as a change
 * is made to keep it synced
 */
export class Dashboard extends React.Component<IOwnProps, IOwnState>{
    private boundHandleNewCustomer: (customer:Customer)=>void;
    private boundHandleNewQuote: (quote:Quote)=>void;
    private boundHandleUpdate: ()=>void;
    private boundHandleStartUpdate: ()=>void;
    private boundShowModal: ()=>void;
    private boundCloseModal: ()=>void;
    constructor(props: any){
        super(props);
        this.state = { 
            customers: [],
            quotes: [],
            loading: true,
            showModal: false,
            showCustomerModal: false
         }
         this.boundHandleNewCustomer = this.handleNewCustomer.bind(this);
         this.boundHandleNewQuote = this.handleNewQuote.bind(this);
         this.boundHandleUpdate = this.handleUpdate.bind(this);
         this.boundHandleStartUpdate = this.handleStartUpdate.bind(this);
         this.boundShowModal = this.handleShowModal.bind(this);
         this.boundCloseModal = this.handleCloseModal.bind(this);
    }

    private handleShowModal(){
        this.setState({
            showModal:true
        })
    }

    private handleCloseModal(){
        this.setState({
            showModal:false
        })
    }

    // set the state to loading to lock the components while fetching
    // has to call a inner function as setstate is async
    private handleStartUpdate(){
        this.setState({
            loading:true
        }, ()=>{
            this.boundHandleUpdate()
        })
    }

    // refetch the state of the application
    private async handleUpdate(){ 
        this.setState({
            quotes: await getQuotes(),
            customers: await getCustomers(),
            loading: false
        })
    }


    // post a new customer, then refetch
    private async handleNewCustomer(customer:Customer){
        await postCustomer(customer)
        await this.handleStartUpdate()
    }

    // post a new quote, then refetch
    private async handleNewQuote(quote:Quote){
        await postQuote(quote)
        await this.handleStartUpdate()
    }

    // fetch data on mount
    componentDidMount() {
        this.handleUpdate();
    }
    render(){
        return(
            <div className="dashboard"><div className={"dashboard-panels"}>
                <div className="refresh-button">
                    <Button onClick={this.boundHandleStartUpdate} variant="outline-secondary"><FontAwesomeIcon icon={faSyncAlt}/></Button>
                </div>
                <PanelWrapper
                    title={"Welcome"}
                    hideNavbar={true}
                    width={"90%"}
                    loading={false}
                >
                    <Welcome/>
                </PanelWrapper>
                <PanelWrapper
                    title={"Quotes"}
                    width={"750px"}
                    icon={<FontAwesomeIcon icon={faQuoteLeft}/>}
                    loading={this.state.loading}
                >
                    <ViewQuotes
                        quotes={this.state.quotes}
                        loading={this.state.loading}
                        handleNewCustomer={this.boundHandleNewCustomer}
                    />
                </PanelWrapper>
                <PanelWrapper
                    title={"Quick Quote"}
                    icon={<FontAwesomeIcon icon={faFastForward}/>}
                    loading={this.state.loading}
                >
                    <NewQuote
                        customers={this.state.customers}
                        handleNewQuote={this.boundHandleNewQuote}
                        loading={this.state.loading}
                    />
                </PanelWrapper>
                <PanelWrapper
                    title={"Customers"}
                    width={"250px"}
                    icon={<FontAwesomeIcon icon={faUsers}/>}
                    buttons={<Button variant="outline-primary" onClick={this.boundShowModal}>+</Button>}
                    loading={this.state.loading}
                >
                    <ViewCustomers
                        customers={this.state.customers}
                        handleNewCustomer={this.boundHandleNewCustomer}
                        loading={this.state.loading}
                    />
                </PanelWrapper>
                <PanelWrapper
                    title={"Top Destinations"}
                    icon={<FontAwesomeIcon icon={faPlaneArrival}/>}
                    width={"325px"}
                    loading={this.state.loading}
                >
                    <Destinations
                        quotes={this.state.quotes}
                    />
                </PanelWrapper>
                <Popout 
                    title={"New Customer"}
                    icon={<FontAwesomeIcon icon={faUserPlus}/>}
                    show={this.state.showModal}
                    handleClose={this.boundCloseModal}
                >
                    <NewCustomer
                        loading={this.state.loading}
                        handleNewCustomer={this.boundHandleNewCustomer}
                    />
                </Popout>
            </div></div>
        )
    }

}