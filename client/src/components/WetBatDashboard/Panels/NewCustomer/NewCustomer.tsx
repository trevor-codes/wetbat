import { faUserCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { Button, Col, Form, Row } from "react-bootstrap";
import { Customer } from "../../../standards/interfaces";
interface IOwnProps{
    loading: boolean
    handleNewCustomer: (customer:Customer)=>void
    customer?: Customer|null
}

/**
 * Component to handle the creating of new customers
 */
export default class NewCustomer extends React.Component<IOwnProps>{
    private boundHandleSubmit: any;
    private boundFirstNameRef: any;
    private boundLastNameRef: any;
    private boundPhoneNumberRef: any;
    constructor(props:IOwnProps){
        super(props);
        this.boundHandleSubmit = this.handleSubmit.bind(this);
        this.boundFirstNameRef = React.createRef();
        this.boundLastNameRef = React.createRef();
        this.boundPhoneNumberRef = React.createRef();
    }

    private handleSubmit(e:any){
        e.preventDefault();
        const customer: Customer = {
                customerId: null,
                firstName: this.boundFirstNameRef.current.value,
                lastName: this.boundLastNameRef.current.value,
                phoneNumber: this.boundPhoneNumberRef.current.value,
            }
        this.props.handleNewCustomer(customer)
    }

    render(){ 
        return ( 
                <>
                    <div style={{display: "flex", justifyContent: "center"}}>
                        <FontAwesomeIcon color={"grey"} icon={faUserCircle} size={"10x"}/>
                    </div>
                { 
                (this.props.customer)?( 
                    <Form onSubmit={this.boundHandleSubmit}>
                        <Row>
                            <Col>
                                <Form.Label>First Name</Form.Label>
                                <Form.Control value={this.props.customer.firstName} disabled/>
                            </Col>
                            <Col>
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control value={this.props.customer.lastName} disabled/>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Label>Phone Number</Form.Label>
                                <Form.Control value={this.props.customer.phoneNumber} disabled/>
                            </Col>
                        </Row>
                    </Form>
                ):(
                    <Form onSubmit={this.boundHandleSubmit}>
                        <Row>
                            <Col>
                                <Form.Label>First Name</Form.Label>
                                <Form.Control ref={this.boundFirstNameRef} required/>
                            </Col>
                            <Col>
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control ref={this.boundLastNameRef} required/>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Label>Phone Number</Form.Label>
                                <Form.Control ref={this.boundPhoneNumberRef} type="tel" required/>
                            </Col>
                            <Col>
                                <Button type="submit" variant="primary">Create Customer</Button>{' '}
                            </Col>
                        </Row>
                    </Form>
                )
            }
            </>
        )
    }
}