import React from "react";
import { Button, Col, Form, Row } from "react-bootstrap";
import { postQuote } from "../../../../api/WetbatAPI";
import { Customer, Quote } from "../../../standards/interfaces";
interface IOwnProps{
    customers: Array<Customer>,
    handleNewQuote: (quote:Quote)=>void,
    loading: boolean
}

interface IOwnState{
    quote: Quote
}

/**
 * Compontent to handle the creation of new Quotes.
 */
export default class NewQuote extends React.Component<IOwnProps, IOwnState>{
    private boundHandleSubmit: (e:any)=>void;
    private boundHandleChange: (e:any)=>void;

    private departureDateRef: any;
    private departureLocationRef: any;
    private destinationDateRef: any;
    private destinationLocationRef: any;
    private peopleRef: any;
    private transportationRef: any;
    private customerRef: any;
    
    constructor(props:IOwnProps){
        super(props)
        this.state={
            quote:{
                quoteId: null,
                departureLocation: '',
                departureDate: '',
                destinationLocation: '',
                destinationDate: '',
                rental: '',
                travellers: 0,
            }
        }
        this.boundHandleSubmit = this.handleSubmit.bind(this);
        this.boundHandleChange = this.handleChange.bind(this);

        this.departureDateRef = React.createRef();
        this.departureLocationRef = React.createRef();
        this.destinationDateRef = React.createRef();
        this.destinationLocationRef = React.createRef();
        this.peopleRef = React.createRef();
        this.transportationRef = React.createRef();
        this.customerRef = React.createRef();
    }


    private handleChange(e:any) {
        console.log( e.target )
        this.setState({
            quote: {
                ...this.state.quote,
                destinationLocation: e.target.value
            }
        });
    }

    private handleSubmit(e:any){
        e.preventDefault();
        const quote: Quote = {
                quoteId: null,
                destinationLocation: this.destinationLocationRef.current.value,
                destinationDate: this.destinationDateRef.current.value,
                departureLocation: this.departureLocationRef.current.value,
                departureDate: this.departureDateRef.current.value,
                travellers: this.peopleRef.current.value,
                rental: this.transportationRef.current.value,
                customer: this.props.customers.find((customer)=>(customer.customerId == this.customerRef.current.value))
            }
        this.props.handleNewQuote(quote)
    }

    render(){
        return(
            <Form onSubmit={this.boundHandleSubmit}>
                <Row>
                    <Col>
                        <Form.Group>
                            <Form.Label>Departure Location</Form.Label>
                            <Form.Control ref={this.departureLocationRef} required/>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Destination Location</Form.Label>
                            <Form.Control ref={this.destinationLocationRef} required/>
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Group>
                            <Form.Label>Departure Date</Form.Label>
                            <Form.Control ref={this.departureDateRef} type="date" required/>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Destination Date</Form.Label>
                            <Form.Control ref={this.destinationDateRef} type="date" required/>
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Group>
                            <Form.Label>Travellers</Form.Label>
                            <Form.Control ref={this.peopleRef} type="number" required/>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Rental</Form.Label>
                            <Form.Control ref={this.transportationRef} required/>
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Group>
                            <Form.Label>Customer</Form.Label>
                            <Form.Control ref={this.customerRef}as="select" defaultValue="Customer" required>
                                <option></option>
                                {
                                    this.props.customers.map((customer)=>(
                                        <option key={customer.customerId} value={`${customer.customerId}`}>{`${customer.firstName} ${customer.lastName}`}</option>
                                    ))
                                }
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Button type="submit" variant="primary">Create Quote</Button>{' '}
                        </Form.Group>
                    </Col>
                </Row>
            </Form>
        )
    }
}