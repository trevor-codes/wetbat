import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { Button, Table } from "react-bootstrap";
import { Popout } from "../../../modal/Popout";
import { Customer, Quote } from "../../../standards/interfaces";
import { enUSFormatter } from "../../../standards/standards";
import NewCustomer from "../NewCustomer/NewCustomer";
interface IOwnProps{
    quotes: any,
    loading: boolean
    handleNewCustomer: (customer:Customer)=>void
}

export default function ViewQuotes(props:IOwnProps) {
    const [show, setShow] = React.useState(false);
    const [customer, setCustomer]:[Customer|null, any] = React.useState(null);

    const handleClose = () => setShow(false);
    const handleShow = (e:any) => { 
        let searchQuote:Quote = props.quotes.find((quote:Quote) => {
            return quote.quoteId == e.target.value
        } )
        setCustomer(searchQuote.customer);
        setShow(true)
    };

    if(props.loading){
        return ( <>loading...</> )
    } else {
    return ( 
        <>
        <Table striped bordered hover size="sm">
            <thead>
                <tr>
                    <th>Departure Date</th>
                    <th>Departure Location</th>
                    <th>Destitation Date</th>
                    <th>Destination Location</th>
                    <th>Travellers</th>
                    <th>Rental</th>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
            {
                props.quotes.map(( quote: Quote ) => {
                    console.log(quote.departureDate);
                    
                    return(
                        <tr key={quote.quoteId}>
                            <td>{enUSFormatter( new Date(quote.departureDate) )}</td>
                            <td>{quote.departureLocation}</td>
                            <td>{enUSFormatter( new Date(quote.destinationDate) )}</td>
                            <td>{quote.destinationLocation}</td>
                            <td>{quote.travellers}</td>
                            <td>{quote.rental}</td>
                            <td><Button variant="link" value={`${quote.customer?.customerId}`} onClick={handleShow}>{`${ quote.customer?.firstName } ${quote.customer?.lastName}`}</Button></td> 
                        </tr>
                    )
                })
            }
            </tbody>
        </Table>
        <Popout
            title={"View Customer"}
            icon={<FontAwesomeIcon icon={faUser}/>}
            show={show}
            handleClose={handleClose}
        >
            <NewCustomer
                loading={props.loading}
                handleNewCustomer={props.handleNewCustomer}
                customer={customer}
            />
        </Popout>
        </>
    )
    }
  }