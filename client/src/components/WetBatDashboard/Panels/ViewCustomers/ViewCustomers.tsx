import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { Button, ListGroup } from "react-bootstrap";
import { Customer } from "../../../standards/interfaces";
import { Popout } from "../../../modal/Popout";
import NewCustomer from "../NewCustomer/NewCustomer";
interface IOwnProps{
    customers: Array<Customer>,
    handleNewCustomer: (customer:Customer)=>void,
    loading: boolean
}

/**
 * View a list of all customers. Selecting a customer will open a Modal to view its details
 * Additionally create new customers as well
 * @param props 
 * @returns 
 */
export default function ViewCustomers(props:IOwnProps) {
    const [show, setShow] = React.useState(false);
    const [customer, setCustomer]:[Customer|null, any] = React.useState(null);

    const handleClose = () => setShow(false);
    const handleShow = (e:any) => { 
        let searchCustomer = props.customers.find((customer:Customer) => {
            return customer.customerId == e.target.title
        } )
        setCustomer(searchCustomer);
        setShow(true)
    };

    if(props.loading){
        return ( <>loading...</> )
    } else {
    return ( 
        <>
        <ListGroup>
             {
                 props.customers.map(( customer: Customer ) => {
                     let name = `${customer.firstName} ${customer.lastName}`;
                     return(
                        <ListGroup.Item
                            key={customer.customerId+name}
                            title={`${customer.customerId}`}
                            onClick={handleShow}
                        >
                            {name}
                        </ListGroup.Item>
                     )
                 })
             }
        </ListGroup>
        <Popout 
            title={"View Customer"}
            icon={<FontAwesomeIcon icon={faUser}/>}
            show={show}
            handleClose={handleClose}
        >
            <NewCustomer
                loading={props.loading}
                handleNewCustomer={props.handleNewCustomer}
                customer={customer}
            />
        </Popout>
        </>
    )
    }
  }