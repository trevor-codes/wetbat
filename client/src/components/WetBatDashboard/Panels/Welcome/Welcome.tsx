import "./Welcome.css"

/**
 * A nice welcome screen :)
 * @param props 
 * @returns 
 */
export default function Welcome(props:any){
    return(
        <div className={"welcome-panel"}>
            <h1>Welcome to your Wetbat Dashboard!</h1>
        </div>
    )
}