import { Tooltip } from "chart.js";
import {ArcElement, CategoryScale, Chart, Legend, LinearScale, PieController, Title } from "chart.js";
import { countBy, keys, values } from "lodash";
import React, { createRef, RefObject, useRef } from "react";
import { Quote } from "../../../standards/interfaces";

interface IOwnProps{
    quotes: Array<Quote>
}
export class Destinations extends React.Component<IOwnProps>{
    private chartRef = createRef<HTMLCanvasElement>();
    constructor(props:IOwnProps){
        super(props)
    }
    componentDidMount(){
        if(this.chartRef.current){
            const countDestinations = countBy(this.props.quotes.map((quote)=>(quote.destinationLocation.toLowerCase())))


            const chart = this.chartRef?.current.getContext("2d")!;
            Chart.register(PieController, CategoryScale, LinearScale, ArcElement, Title, Legend, Tooltip)
            const data = {
            labels: keys(countDestinations),
                datasets: [{
                    data: values(countDestinations),
                    backgroundColor: [
                    '#648FFF',
                    '#785EF0',
                    '#DC267F',
                    '#FE6100',
                    '#FFB000'
                    ],
                    hoverOffset: 4
                }]
            };
            const destinationsChart = new Chart(chart,{
                type: "pie",
                data
            })

        }
    }
    render(){
        return(
            <canvas id="destination-chart" ref={this.chartRef}></canvas>
        )
    }
}