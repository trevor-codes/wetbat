import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faQuoteLeft } from '@fortawesome/free-solid-svg-icons'
import { ButtonToolbar, Nav, Navbar } from "react-bootstrap";
import "./PanelWrapper.css"

interface IOwnProps{
    icon?: any,
    title:string,
    loading:boolean,
    hideNavbar?: boolean,
    buttons?: any,
    width?:string,
    children?:any
}

/**
 * Generic wrapper to make dashboard panels. Takes any component as a child.
 * @param props 
 * @returns 
 */
export default function PanelWrapper(props:IOwnProps){
    return(
        <div className="panel-wrapper" style={props.width?{width:props.width}:{}}>
            { !props.hideNavbar ? ( 
            <div className="panel-wrapper-header">
            <Navbar bg="light" variant="light" expand="lg" >
                <Nav>
                    <Navbar.Brand>
                        {props.icon}
                        {props.title}
                    </Navbar.Brand>
                </Nav>
                <Nav className="ml-auto">
                    <ButtonToolbar>{ props.buttons }</ButtonToolbar>
                </Nav>
            </Navbar>
            </div>
            ) : ( <></> ) }
            <div className="panel-wrapper-body">
                { props.loading ? "loading..." : props.children }
            </div>
        </div>
    )
}