// format a date as the US format
export function enUSFormatter(date:Date){
    const enUSFormatter = new Intl.DateTimeFormat('en-US');
    return enUSFormatter.format(date)
}