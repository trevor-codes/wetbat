export interface Quote{
    quoteId: number|null,
    departureLocation: string,
    departureDate: string,
    destinationLocation: string,
    destinationDate: string,
    travellers: number,
    rental: string,
    customer?: Customer,
    quoteCustomerId?: number
}

export interface Customer{
    customerId: number|null,
    firstName: string,
    lastName: string,
    phoneNumber: string,
    quotes?: Array<Quote>
}