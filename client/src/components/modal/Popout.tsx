import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import { Button, Modal } from "react-bootstrap";

interface IOwnProps{
    icon?: any,
    title: string,
    children?: any

    show:boolean
    handleClose: ()=>void
}

/**
 * Generic bootstrap popout modal wrapper for a component.
 * @param props 
 * @returns 
 */
export function Popout(props:IOwnProps) {
  return (
    <>
      <Modal show={props.show} onHide={props.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{props.icon}</Modal.Title>
          <Modal.Title>{props.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{props.children}</Modal.Body>
      </Modal>
    </>
  );
}