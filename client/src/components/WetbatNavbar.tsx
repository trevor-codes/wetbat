import { faBell, faCog, faCommentAlt, faTint } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, ButtonGroup, ButtonToolbar, Container, Form, FormControl, Nav, Navbar } from "react-bootstrap";

export default function():JSX.Element{
    return(
        <Navbar collapseOnSelect expand="lg" bg="primary" variant="dark">
            <Container>
                <Navbar.Collapse>
                    <Nav>
                        <Navbar.Brand><FontAwesomeIcon icon={faTint}/></Navbar.Brand>
                        <Navbar.Brand>Wet Bat</Navbar.Brand>
                    </Nav>
                    <Nav className="ml-auto">
                        <Form inline>
                            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                            <Button variant="light">Search</Button>
                        </Form>
                        <Form inline>
                            <Button variant="outline-light">
                                <FontAwesomeIcon icon={faBell}/>
                            </Button>
                            <Button variant="outline-light">
                                <FontAwesomeIcon icon={faCommentAlt}/>
                            </Button>
                            <Button variant="outline-light">
                                <FontAwesomeIcon icon={faCog}/>
                            </Button>
                    </Form>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}