import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Dashboard } from './components/WetBatDashboard/Dashboard';
import WetbatNavbar from './components/WetbatNavbar';

function App() {
  return (
    <div className="App">
      <WetbatNavbar/>
      <Dashboard/>
    </div>
  );
}

export default App;
