import { Customer, Quote } from "../components/standards/interfaces";

const quoteAPI = `http://localhost:8080`

async function getAPI(endpoint:string) {
  const quotes = await fetch(quoteAPI+endpoint, { 
        method: "GET", 
        headers: {
            'Content-Type': 'application/json'
        },
    });
  const json = await quotes.json();
  
  return json||[];
}

export async function getQuotes() {
  return await getAPI(`/quotes`);
};

export async function getCustomers() {
  return await getAPI(`/customers`);
};

async function postAPI(endpoint:string, data:object) {
  const quotes = await fetch(quoteAPI+endpoint, { 
        method: "post", 
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
  const json = await quotes.json();
  return json||[];
}

export async function postQuote(data:Quote) {
  return await postAPI(`/quote`, data);
};

export async function postCustomer(data:Customer) {
  return await postAPI(`/customer`, data);
};